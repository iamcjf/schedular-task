build:
docker build -t schedular .
exec:
docker run -d -v /etc/localtime:/etc/localtime:ro -e "TZ=Asia/Taipei" schedular
